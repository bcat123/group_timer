<?php

// https://www.php.net/manual/en/function.mail.php

$group = $_POST['group']; 
$name1 = $_POST['name1'];
$name2 = $_POST['name2'];
$name3 = $_POST['name3'];
$timer1 = $_POST['timer1'];
$timer2 = $_POST['timer2'];
$timer3 = $_POST['timer3'];
$notes = $_POST['notes'];
$emailTo = $_POST['emailTo'];
$passcode = $_POST['passcode'];

// $email_body = "Group $group\n\n$name1: $timer1\n$name2: $timer2\n$name3: $timer3\n\n$notes\n";
$email_body = <<< _END
    <h2>Group $group</h2>
    <table>
    <tr><th>Member</th><th>Time</th></tr>
    <tr><td>$name1</td><td>$timer1</td></tr>
    <tr><td>$name2</td><td>$timer2</td></tr>
    <tr><td>$name3</td><td>$timer3</td></tr>
    </table>
    <h2>Notes</h2>
$notes
_END;


$email_subject = "Presentation: Group $group";

// Set content-type header for sending HTML email 
$headers = "MIME-Version: 1.0" . "\r\n"; 
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n"; 

$headers .= 'From: no-reply@montana.edu' . "\r\n" .
    'Reply-To: no-reply@montana.edu' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

if ($passcode == "Ch@ngeMe!") {
    mail($emailTo,$email_subject,$email_body,$headers);
echo <<<_END
    Email sent to $emailTo<br>
    (Check the Junk folder.)
    $email_body
_END;
    
}
else {
    echo "Wrong passcode.<br>";
    echo "
    <form method='GET' name='emailform' action='form_to_email.php'>
    <input type='hidden' name='group' value=$group>
    <input type='hidden' name='name1' value='$name1'>
    <input type='hidden' name='name2' value='$name2'>
    <input type='hidden' name='name3' value='$name3'>
    <input type='hidden' name='timer1' value='$timer1'>
    <input type='hidden' name='timer2' value='$timer2'>
    <input type='hidden' name='timer3' value='$timer3'>
    <input type='hidden' name='notes' value='$notes'>
    <input id='passcode' name='passcode' type='password'><br>
    <input type='submit' value='Resend'></button></div></form>
    ";
}
?>