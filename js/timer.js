btn1 = document.getElementById("start1");
btn1.addEventListener("click", startTimer);
btn2 = document.getElementById("start2");
btn2.addEventListener("click", startTimer);
btn3 = document.getElementById("start3");
btn3.addEventListener("click", startTimer);
console.log(btn3);

btnPause = document.getElementById("pauseBtn");
btnPause.addEventListener("click", pauseTimer);

input1 = document.getElementById("name1");
input1.addEventListener("keyup", setPresenter);
input2 = document.getElementById("name2");
input2.addEventListener("keyup", setPresenter);
input3 = document.getElementById("name3");
input3.addEventListener("keyup", setPresenter);

let elapsed = 0;
let startId = '';

// https://developer.mozilla.org/en-US/docs/Web/API/setInterval
let isTiming; // null; intervalID

function resetButtonStyles(){
    btn1.style = "revert";
    btn2.style = "revert";
    btn3.style = "revert";
    btnPause.style = "revert";
}

function startTimer(e) {
    startId = e.target.id;
    btn = document.getElementById(startId);
    resetButtonStyles()
    btn.style.backgroundColor = "lightgreen";
    if (!isTiming){
        isTiming = setInterval(clockTick, 1000); // returns an ID
    }
  }

  function pauseTimer() { 
      resetButtonStyles()
      btnPause.style.backgroundColor = "tomato";
      btnPause.style.color = "white";
      btnPause.style.textTransform="uppercase";
      clearInterval(isTiming); 
      isTiming = null;
  }

  function setPresenter(e) {
      if (e.target.id === "name1") {
        document.getElementById("start1").innerHTML = input1.value;
        if (input1.value == ''){
            document.getElementById("start1").innerHTML = "Presenter 1";
        }
      }
      if (e.target.id === "name2") {
        document.getElementById("start2").innerHTML = input2.value;
        if (input2.value == ''){
            document.getElementById("start2").innerHTML = "Presenter 2";
        }
      }
      if (e.target.id === "name3") {
        document.getElementById("start3").innerHTML = input3.value;
        if (input3.value == ''){
            document.getElementById("start3").innerHTML = "Presenter 3";
        }
      }
  }

let time1 = 0;
let elapsed1 = "0:0:0"
let time2 = 0;
let elapsed2 = "0:0:0"
let time3 = 0;
let elapsed3 = "0:0:0"
function clockTick(e) {

    if (startId == "start1") {
       time1 += 1;
       elapsed1 = formatTime(time1);
       document.getElementById("timer1").innerHTML = elapsed1;
       document.getElementById("formtimer1").value = elapsed1;
    }
    else if (startId == "start2") {
        time2 += 1;
        elapsed2 = formatTime(time2);
        document.getElementById("timer2").innerHTML = elapsed2;
        document.getElementById("formtimer2").value = elapsed2;
    }
    else if (startId == "start3") {
        time3 += 1;
        elapsed3 = formatTime(time3);
        document.getElementById("timer3").innerHTML = elapsed3;
        document.getElementById("formtimer3").value = elapsed3;
    }
}

function formatTime(seconds){
    let hrs = Math.floor(seconds/3600)
    let min = Math.floor(seconds / 60) % 60;
    let sec = seconds % 60;
    if (sec < 10) {
        sec = "0" + sec
    }
    if (min < 10) {
        min = "0" + min
    }
    elapsed = hrs + ':' + min + ':' + sec;
    return elapsed;
}