# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Webpage provides multiple timers for timing multiple things at once, then let's you email the results.
* v0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

Clone (download) the group_timer repository, and place into a folder of your own. The app runs on a webserver, so set up localhost if you're working on your own computer (AMPPS is a good choice for Mac or PC). Or host it on the internet. There's a passcode to set up in the form_to_email.php file.

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin: Daniel DeFrance
* Other community or team contact
